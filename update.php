<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<?php 
include("./vendor/autoload.php");
$id = $_GET["id"];
$animal = new App\Models\Animal();
$data = $animal->getSingleData($id);

?>

<body>
    <div class="container mt-3">
        <div class="card mb-3" style="width: 40vw;margin: auto;">
            <div class="card-body">
                <div class="card-body">
                    <form action="./crud.php?id=<?=$id?>" method="POST">
                        <div class="input-group mb-3">
                            <label class="input-group-text" for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="name" value="<?= $data["name"]?>">
                        </div>
                        <div class="input-group mb-3">
                            <label class="input-group-text" for="legs">Legs</label>
                            <input class="form-control" type="text" name="leg" id="legs" value="<?= $data["leg"]?>">
                        </div>
                        <input class="btn btn-primary" type="submit" name="btn" value="update">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>