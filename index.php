<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Class Work</title>
  </head>
  <body>
    <?php 
    include_once("./vendor/autoload.php");
    $animalObj = new App\Models\Animal();
    $dataTable = $animalObj->getData();
    $message = $animalObj->getMessage();
    ?>
    <div class="container mt-3">
        <?php if($message): ?>
        <div class="alert alert-<?php echo $message[1]; ?> text-center" 
            style="width: 60vw;margin: auto;" role="alert">
            <?php echo $message[0]; ?>
        </div>
        <?php endif; ?>
        <form action="./add.php" method="POST">
            <button class="btn btn-primary">Add</button>
        </form>
        <div class="card" style="width: 60vw;margin: auto;"> 
            <table class="table">
                <tr>
                    <th>Animal Name</th>
                    <th>Number of Legs</th>
                    <th>Options</th>
                </tr>
                <?php
                foreach ($dataTable as $key => $animal) {
                    echo "<tr>";
                    echo "<td>".$animal["name"]."</td>";
                    echo "<td>".$animal["leg"]."</td>";
                    echo <<<EOL
                    <form action="./crud.php?id=$key" method="POST">
                        <td>
                            <button name="btn" value="show" class="btn btn-outline-info m-2">Show</button>
                            <button name="btn" value="edit" class="btn btn-outline-info m-2">Update</button>
                            <button name="btn" value="delete" class="btn btn-outline-danger">Delete</button>
                        </td>
                    </form>
                    EOL;
                    echo "</tr>";
                }
                $animalObj->clearMessage(); 
                ?>
            </table>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>