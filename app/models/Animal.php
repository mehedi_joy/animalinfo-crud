<?php

namespace App\Models;

class Animal {
    public $name;
    public $leg;
    public $dataTable;
    public $message;

    function __construct() {
        session_start();
    }

    // function __destruct() {
    //     unset($_SESSION["message"]);
    // }

    public function saveAnimal($data) {
       $_SESSION["animals"][] = $data;
       $_SESSION["message"] = ["Animal Saved Successfully", "primary"];
    }

    public function getData() {
        if(isset($_SESSION["animals"])) {
            return ($_SESSION["animals"]);
        } else {
            return [];
        }
    }

    public function deleteData($id) {
        unset($_SESSION["animals"][$id]);
        $_SESSION["message"] = ["Animal Deleted Successfully","danger"];
    }

    public function getSingleData($id) {
        return $_SESSION["animals"][$id];
    }

    public function updateAnimal($id, $data) {
        $_SESSION["animals"][$id] = $data;
        $_SESSION["message"] = ["Animal Updated Successfully", "success"];
    }

    public function getMessage() {
        if(isset($_SESSION["message"])) {
            return $_SESSION["message"];
        } else {
            return null;
        }
    }

    public function clearMessage() {
        unset($_SESSION["message"]);
    }
}