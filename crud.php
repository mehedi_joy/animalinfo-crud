<?php
include("./vendor/autoload.php");

$btn_val = $_POST["btn"];

if(isset($_GET["id"])) {
    $id = $_GET["id"];
}

$message;

if($btn_val == "delete") {
    $animal = new App\Models\Animal();
    $animal->deleteData($id);
} else if($btn_val == "edit") {
    header("Location: ./update.php?id=$id");
    die();
} else if($btn_val == "update") {
    $animal = new App\Models\Animal();
    $animal->updateAnimal($id, $_POST);
} else if($btn_val == "save") {
    $animal = new App\Models\Animal();
    $animal->saveAnimal(["name" => $_POST["name"],"leg" => $_POST["leg"]]);
} else if($btn_val == "show") {
    header("Location: ./show.php?id=$id");
    die();
}

header("Location: ./index.php");